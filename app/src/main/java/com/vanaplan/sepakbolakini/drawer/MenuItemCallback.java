package com.vanaplan.sepakbolakini.drawer;

import java.util.List;

/**
 * Vanaplan
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author vanaplan
 * Copyright 2018
 */
public interface MenuItemCallback {

    void menuItemClicked(List<NavItem> action, int menuItemId, boolean requiresPurchase);
}
