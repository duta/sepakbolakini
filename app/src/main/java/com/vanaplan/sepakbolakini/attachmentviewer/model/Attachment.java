package com.vanaplan.sepakbolakini.attachmentviewer.model;

import java.io.Serializable;

/**
 * Vanaplan
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author vanaplan
 * Copyright 2018
 */
public abstract class Attachment implements Serializable {

    public abstract String getDescription();
}
